package com.collabera.exceptions;

public class AccountNotExistException  extends RuntimeException{
	
	private static final long serialVersionUID = -7902410622952982837L;

	public AccountNotExistException() {
		super("No Account Exist!");
	}

}
