package com.collabera.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler{


  @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {

	    ErrorResponse error = new ErrorResponse("Validation Failed","", getFieldErrors(ex));
	    return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
    }
	 
  	@ExceptionHandler(BindException.class)
	public ResponseEntity<?> handleBindException(BindException ex) {
	    ErrorResponse error = new ErrorResponse("Validation Failed", "", getFieldErrors(ex));
	    return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleAllExceptions(Exception ex) {
	    ErrorResponse error = new ErrorResponse("Server Error", ex.getLocalizedMessage(), new HashMap<>());
	    return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
    
    public Map<String, String> getFieldErrors(MethodArgumentNotValidException ex){
    	 Map<String,String> map = new HashMap<>();
			for(FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return map;
    }
    
    public Map<String, String> getFieldErrors(BindException ex){
   	 Map<String,String> map = new HashMap<>();
			for(FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return map;
   }
}
