package com.collabera.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.collabera.entities.Account;
import com.collabera.entities.Type;

/**
 * @author Edison Ray I.Tañala
 *
 */
public interface AccountRepository extends JpaRepository<Account, Integer>{

	List<Account> findByType(Type customer);

	Account findByUsernameAndPassword(String username, String password);

	Account findByUsername(String username);

	
}
