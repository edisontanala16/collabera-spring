package com.collabera.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.collabera.entities.Account;
import com.collabera.entities.Type;
import com.collabera.exceptions.AccountNotExistException;
import com.collabera.exceptions.PasswordMismatchException;
import com.collabera.repositories.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepo;
	
	private PasswordEncoder passwordEncoder = new  BCryptPasswordEncoder();


	@Override
	public List<Account> findByCustomer(){
		return accountRepo.findByType(Type.CUSTOMER);
	}
	

	@Override	
	public Account save(Account account) {
		account.setType(Type.CUSTOMER);
		if (account.getPassword().length() > 0) {
			account.setPassword(passwordEncoder.encode(account.getPassword()));
		}
		return accountRepo.save(account);
	}

	@Override	
	public Account findById(Integer id) {
		Optional<Account> account =  accountRepo.findById(id);
		return account.orElse(null);
	}

	@Override
	public void delete(Account account) {
		accountRepo.delete(account);
	}
	
	
	public String findByCredentials(String username,String password) throws AccountNotExistException,PasswordMismatchException  {	
			Account account = accountRepo.findByUsername(username);
		
			if(account == null) throw new AccountNotExistException();	
			if(!passwordEncoder.matches(password, account.getPassword())) throw new PasswordMismatchException();
			
			return "Your Credentials is " + account.getType()+" account.";
		
	}

	
}
