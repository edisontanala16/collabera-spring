package com.collabera.services;

import java.util.List;

import com.collabera.entities.Order;

public interface OrderService {

	List<Order> findAll();
	List<Order> findByCustomerId(Integer id);
	Order findByOrderNo(String orderNo);
	Order save(Order order);
}
