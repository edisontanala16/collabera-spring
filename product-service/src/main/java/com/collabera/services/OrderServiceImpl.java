package com.collabera.services;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.collabera.entities.Account;
import com.collabera.entities.Order;
import com.collabera.entities.OrderDetail;
import com.collabera.entities.OrderStatus;
import com.collabera.entities.Payment;
import com.collabera.entities.PaymentStatus;
import com.collabera.repositories.OrderDetailRepository;
import com.collabera.repositories.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepo;
	
	@Autowired
	private OrderDetailRepository detailRepo;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Order> findAll() {
		return orderRepo.findAll();
	}

	@Override
	public List<Order> findByCustomerId(Integer id) {
		return orderRepo.findByCustomerId(id);
	}

	@Override
	public Order findByOrderNo(String orderId) {
		
		Order order = orderRepo.findByOrderNo(orderId);          
		ResponseEntity<Account> resEntity = restTemplate.getForEntity("http://CUSTOMER-SERVICE/customers/findbyId/"+order.getCustomerId(), Account.class);
		
		if(resEntity.getStatusCode() == HttpStatus.OK) {
			order.setAccount(resEntity.getBody());
		}	
		return order;
	}

	@Override
	@Transactional
	public Order save(Order order) {
		
		double amount = 0.0;
		for(OrderDetail detail :order.getProducts()) {
			detail.setOrder(order);
			detailRepo.save(detail);
			amount += detail.getPrice() * detail.getQuantity();
		}
		order.setTransactionDate(LocalDate.now());
		order.setAmount(amount);
		order.setBalanceAmount(amount);
		order.setPaymentStatus(PaymentStatus.UN_PAID);
		order.setOrderStatus(OrderStatus.PENDING);
	
		return orderRepo.save(order);
	}

	public Order payment(@Valid Payment payment) {	
		Order order = findByOrderNo(payment.getOrderNo());
		order.setPaymentRefNo(payment.getPaymentRefNo());
		order.setPaymentType(payment.getPaymentType());
		order.setBalanceAmount(order.getBalanceAmount()-payment.getAmount());
		order.setRemarks(payment.getRemarks());
		
		if(order.getBalanceAmount()==order.getAmount()) {
			order.setPaymentStatus(PaymentStatus.UN_PAID);
		}else if(order.getBalanceAmount() > 0) {
			order.setPaymentStatus(PaymentStatus.PARTIAL);
		}else if(order.getBalanceAmount() == 0) {
			order.setPaymentStatus(PaymentStatus.PAID);
		}else {
			order.setPaymentStatus(PaymentStatus.OVER_PAID);
		}
		return orderRepo.save(order);
	}
	
	

	
}
