package com.collabera.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.collabera.entities.Order;
import com.collabera.entities.PaymentStatus;

/**
 * @author Edison Ray I.Tañala
 *
 */
public interface OrderRepository extends JpaRepository<Order, Integer>{

	List<Order> findByCustomerId(Integer id);

	List<Order> findByPaymentStatus(PaymentStatus unPaid);

	Order findByOrderNo(String orderNo);
	
}
