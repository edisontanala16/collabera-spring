package com.collabera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.collabera.entities.OrderDetail;

/**
 * @author Edison Ray I.Tañala
 *
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer>{
	
}
