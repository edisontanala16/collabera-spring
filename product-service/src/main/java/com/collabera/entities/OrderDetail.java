package com.collabera.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Edison Ray I.Tañala
 *
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	@JsonIgnore
	@ManyToOne
	private Order order;
	
	@NotBlank(message = "Product Name is required")
	private String productName;

	private Integer quantity;
	
	private Double price;
	
}
