package com.collabera.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Edison Ray I.Tañala
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

	private int id;
	private String firstname;
	
	private String lastname;
	
	private String email;
	
}
