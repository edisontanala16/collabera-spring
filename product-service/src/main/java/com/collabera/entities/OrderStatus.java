package com.collabera.entities;
/**
 * @author Edison Ray I.Tañala
 *
 */
public enum OrderStatus {
	PENDING,IN_PROGRESS,COMPLETED
}
