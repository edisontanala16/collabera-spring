package com.collabera.entities;
/**
 * @author Edison Ray I.Tañala
 *
 */
public enum PaymentStatus {
	UN_PAID,PARTIAL,PAID,OVER_PAID
}
