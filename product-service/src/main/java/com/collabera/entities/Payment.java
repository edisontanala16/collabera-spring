package com.collabera.entities;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Edison Ray I.Tañala
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {

	
	@NotBlank(message = "Order No is required")
	private String orderNo;

	@NotBlank(message = "Reference No. is required")
	private String paymentRefNo;
	
	@NotBlank(message="Payment Type is required")
	private String paymentType;
	private Double amount;
	private String remarks;	

}
