package com.collabera.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.collabera.entities.Order;
import com.collabera.entities.Payment;
import com.collabera.entities.PaymentStatus;
import com.collabera.services.OrderServiceImpl;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	private OrderServiceImpl service;
	
	@GetMapping("/findAll")
	public ResponseEntity<List<Order>> findOrders(){
		return new ResponseEntity<List<Order>>(service.findAll(),HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<Order> saveAccount(@Valid @RequestBody Order order){
		return new ResponseEntity<Order>(service.save(order),HttpStatus.OK);
	}
	
	@GetMapping("/findByCustomerId/{id}")
	public ResponseEntity<List<Order>> findByCustomerId(@PathVariable Integer id){
		return new ResponseEntity<List<Order>>(service.findByCustomerId(id),HttpStatus.OK);
	}
	
	@GetMapping("/findByOrderId/{orderNo}")
	public ResponseEntity<Order> findByOrderId(@PathVariable String orderNo){
		return new ResponseEntity<Order>(service.findByOrderNo(orderNo),HttpStatus.OK);
	}
	
	
	@PostMapping("/payment")
	public ResponseEntity<Order> saveAccount(@Valid @RequestBody Payment payment){
		return new ResponseEntity<Order>(service.payment(payment),HttpStatus.OK);
	}	
	
	
}
