package com.collabera.gatewayservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class FallbackController {
	
	@RequestMapping("/customerFallback")
	Mono<String> customerServiceFallback() {
		return Mono.just("Customer Service is taking too long to respond or is down. Pls try after some time!!");
	}
	
	
	@RequestMapping("/adminFallback")
	public Mono<String> userServiceFallback() {
		return Mono.just("Admin Service is taking too long to respond or is down. Pls try after some time!!");
	}
	
	@RequestMapping("/orderFallback")
	public Mono<String> orderFallback() {
		return Mono.just("Product Service is taking too long to respond or is down. Pls try after some time!!");
	}

}
