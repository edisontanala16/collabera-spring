package com.collabera.services;

import java.util.List;

import com.collabera.entities.Account;

public interface AccountService {

	List<Account> findByAdministrator();
	Account save(Account account);
	Account	findById(Integer id);
	void delete(Account account);
	String findByCredentials(String username, String password);
}
