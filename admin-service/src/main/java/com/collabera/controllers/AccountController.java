package com.collabera.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.collabera.entities.Account;
import com.collabera.services.AccountServiceImpl;

@RestController
@RequestMapping("/administrator")
public class AccountController {

	@Autowired
	private AccountServiceImpl service;
	
	@GetMapping("/findAll")
	public ResponseEntity<List<Account>> findbyAdministrator(){
		return new ResponseEntity<List<Account>>(service.findByAdministrator(),HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<Account> saveAccount(@Valid @RequestBody Account account){
		return new ResponseEntity<Account>(service.save(account),HttpStatus.OK);
	}
	
	@GetMapping("/findbyId/{id}")
	public ResponseEntity<Account> findById(@PathVariable Integer id){
		return new ResponseEntity<Account>(service.findById(id),HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable Integer id){
		Account account = service.findById(id);
		if(account !=null) {
			service.delete(account);	
		}
		
	}
	
	@GetMapping("/findByCredentials")
	public ResponseEntity<String> findByCredentials(@RequestParam String username,
													 @RequestParam String password){
		return new ResponseEntity<String>(service.findByCredentials(username, password),HttpStatus.OK);	
	}
	
	
	
}
