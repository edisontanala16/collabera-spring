package com.collabera.exceptions;

public class PasswordMismatchException  extends RuntimeException{
	
	private static final long serialVersionUID = -7902410622952982837L;

	public PasswordMismatchException() {
		super("Password is Incorrect");
	}

}
