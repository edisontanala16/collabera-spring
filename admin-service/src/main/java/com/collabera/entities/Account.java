package com.collabera.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Edison Ray I.Tañala
 *
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	
	@NotBlank(message = "firstname is required")
	private String firstname;
	
	@NotBlank(message = "lastname is required")
	private String lastname;
	
	@Email(message = "must be a valid email")
	@NotBlank(message = "email is required")
	private String email;
	
	@NotBlank(message = "username is required")
	private String username;

	@NotBlank(message = "password is required")
	private String password;

	@Enumerated(EnumType.STRING)
	private Type type;
	
	@Enumerated(EnumType.STRING)	
	private AccountStatus status;
}
