package com.collabera.entities;
/**
 * @author Edison Ray I.Tañala
 *
 */
public enum AccountStatus {
	PENDING,ACTIVE
}
